'use strict';

const mysql = require('mysql');

class MysqlUtils {
    constructor(host, user, password) {
        this.connection = mysql.createConnection({
            host: host,
            user: user,
            password: password,
        });
    }

    connect() {
        return new Promise((resolve, reject) => {
            console.log(`start connect to db`)
            this.connection.connect(err => {
                if (err) reject('error connection: ' + err);
                console.log('connected as id ' + this.connection.threadId);
                resolve();
            });
        });
    }

    createDB(database) {
        return new Promise((resolve, reject) => {
            let sql = `CREATE DATABASE \`${database}\``
            this.connection.query(sql, (err, result) => {
                if (err) {
                    if (err.errno === 1007) {
                        console.log(`DB ${database} exist`);
                        resolve();
                    } else {
                        console.log(err);
                        reject(err);
                    }
                } else {
                    console.log(`DB ${database} created`);
                    resolve();
                }
            });
        });
    }

    switchDB(database) {
        return new Promise((resolve, reject) => {
            let sql = `USE \`${database}\``
            this.connection.query(sql, (err, result) => {
                if (err) reject(err);
                console.log(`Switched to DB ${database}`);
                resolve();
            });
        });
    }

    resetTable(table) {
        return new Promise((resolve, reject) => {
            this.dropTable(table)
                .then(() => this.createTable(table))
                .then(() => resolve())
                .catch(err => reject());
        });
    }

    createTable(table) {
        return new Promise((resolve, reject) => {
            let tableName = table.name;
            let columns = table.columns;
            let sql = `CREATE TABLE \`${tableName}\` (`
            sql += ` \`id\` INT(11) NOT NULL AUTO_INCREMENT ,`;
            let columnDefinitions = [];
            for (let column of columns) {
                let columnDefinition = `\`${column.name}\` ${column.type}`;
                if (column.length) {
                    columnDefinition += `(${column.length})`;
                }
                if (!column.nullable) {
                    columnDefinition += ' NOT NULL';
                }
                columnDefinitions.push(columnDefinition);
            }
            sql += `${columnDefinitions.join()}, PRIMARY KEY (\`id\`))`;
            this.connection.query(sql, (err, result) => {
                if (err) {
                    reject(err);
                } else {
                    console.log(`Table ${tableName} created`);
                    resolve();
                }
            });
        });
    }

    dropTable(table) {
        return new Promise((resolve, reject) => {
            let sql = `DROP TABLE IF EXISTS \`${table.name}\``
            this.connection.query(sql, (err, result) => {
                if (err) reject(err);
                console.log(`DROP TABLE IF EXISTS ${table.name}`);
                resolve();
            });
        });

    }

    save(table, obj) {
        return new Promise((resolve, reject) => {
            let sql = `INSERT INTO ${table} set ?`;
            this.connection.query(sql, obj, (err, result) => {
                if (err) reject(err);
                console.log('Insert data');
                resolve();
            });
        });
    }

    findAll(table) {
        return new Promise((resolve, reject) => {
            let sql = `SELECT * FROM ${table}`;
            this.connection.query(sql, (err, rows) => {
                if (err) reject(err);
                console.log('Find all');
                resolve(rows);
            });
        });
    }

    findOne(table, id) {
        return new Promise((resolve, reject) => {
            let sql = `SELECT * FROM ${table} WHERE id=?`;
            this.connection.query(sql, [id], (err, rows) => {
                if (err) reject(err);
                console.log(`Find ${id}`);
                resolve(rows);
            });
        });
    }

    update(table, id, obj) {
        return new Promise((resolve, reject) => {
            let sql = `UPDATE ${table} set ? WHERE id = ?`;
            this.connection.query(sql, [obj, id], (err, rows) => {
                if (err) reject(err);
                console.log(`UPDATE ${id}`);
                resolve();
            });
        });
    }

    delete(table, id) {
        return new Promise((resolve, reject) => {
            let sql = `DELETE FROM ${table} WHERE id = ?`;
            this.connection.query(sql, [id], (err, rows) => {
                if (err) reject(err);
                console.log(`DELETE ${id}`);
                resolve();
            });
        });
    }
}

module.exports.MysqlUtils = MysqlUtils;
