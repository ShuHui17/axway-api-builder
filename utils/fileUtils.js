const archiver = require('archiver');
const fs = require('fs');
const path = require('path');
const uuid = require('uuid');
const Mustache = require('mustache');

const temp = '/tmp/proxy';
const tempZip = `${temp}/target.zip`;

module.exports = {
    zip,
    renderTemplate
};

function zip(directories, files, folderName) {
    return new Promise((resolve, reject) => {
        let output = fs.createWriteStream(tempZip);
        let archive = archiver('zip', {
            zlib: {
                level: 9
            }
        });
        output.on('close', () => {
            console.log(`Zip '${tempZip}' done (${archive.pointer()} total bytes).`);
            resolve(tempZip);
        });
        output.on('end', () => {
            console.log('Data has been drained');
        })
        archive.on('error', err => reject(err));
        archive.pipe(output);
        for (let directory of directories) {
            archive.directory(directory, folderName);
        }
        for (let file of files) {
            archive.file(file.path, {
                name: `${folderName}/${file.dest}`
            })
        }
        archive.finalize();
    });
}

function renderTemplate(params, templateName, filePath, destPath) {
    return new Promise((resolve, reject) => {
        let template = fs.readFileSync(templateName, 'utf8');
        let rendered = Mustache.render(template, params);
        fs.writeFile(filePath, rendered, err => {
            if (err) {
                reject(err);
            }
            console.log(`render template ${templateName}`);
          if(destPath) {
            resolve({
              filePath: filePath,
              destPath: destPath
            });
          }else{
            resolve({
              filePath: filePath,
              destPath: path.basename(filePath)
          });
          }
        });
    });
}
