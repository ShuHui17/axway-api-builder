const path = require('path');

class File {
  constructor(fullPath,dest){
    this.path = fullPath;
    if(dest){
      this.dest = dest;
    }else{
      this.dest = path.basename(fullPath);
    }
  }
}

module.exports.File = File;
