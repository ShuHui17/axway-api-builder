'use strict';
const fs = require('fs');
const Mustache = require('mustache');
const express = require('express');
const bodyParser = require('body-parser');
const yaml = require('js-yaml');
const {MysqlUtils} = require('./utils/mysqlUtils');
const {File} = require('./model/file');
const fileUtils = require('./utils/fileUtils');

const tempDirectory = '/tmp/proxy';
const tempAppFile = `${tempDirectory}/app.js`;

const app = express();
const config = yaml.safeLoad(fs.readFileSync('config.yaml','utf-8'));
app.use(bodyParser.json());
app.post('/db', (req, res) => {
    let params = req.body;
    Promise.all([generatePackage(params)].concat(params.map(createDB)))
        .then(([file]) => {
            res.download(file);
        }).catch(err => {
            res.setHeader('Content-Type', 'application/json');
            res.send(JSON.stringify(err));
        });
});

app.listen(3000);

function createDB(instance) {
    return new Promise((resolve, reject) => {
        let mysqlUtils = new MysqlUtils(instance.host, instance.user, instance.password);
        mysqlUtils.connect()
            .then(() => mysqlUtils.createDB(instance.database))
            .then(() => mysqlUtils.switchDB(instance.database))
            .then(() => mysqlUtils.resetTable(instance.table))
            .then(() => {
                mysqlUtils.connection.destroy();
                resolve();
            })
            .catch(err => {
                mysqlUtils.connection.destroy();
                reject(err);
            });
    });
}

function generatePackage(tables) {
    return new Promise((resolve, reject) => {
        if (!fs.existsSync(tempDirectory)) {
            fs.mkdirSync(tempDirectory);
        }
        let templateTasks = [];
        templateTasks.push(fileUtils.renderTemplate({
            tables: tables
        }, './template/index.mustache', tempAppFile));
        for (let table of tables) {
            let tempFilePath = `${tempDirectory}/${table.table.name}Router.js`;
            let tempDestPath = `/routes/${table.table.name}Router.js`
            templateTasks.push(fileUtils.renderTemplate(table, './template/route.mustache', tempFilePath, tempDestPath));
        }
        Promise.all(templateTasks)
            .then(filePaths => {
                let files = config.files.map(file => new File(file.path,file.dest)).concat(filePaths.map(file => new File(file.filePath, file.destPath)));
                return fileUtils.zip([], files, 'proxyServer');
            })
            .then(file => resolve(file))
            .catch(err => reject(err));
    });
}
