const JWT = require('jsonwebtoken');

const secret = 'api-builder';

module.exports = {
    generateJWTToken,
    verifyJWTToken,
};


function generateJWTToken(user, hours) {
    let token = JWT.sign({
        exp: Math.floor(Date.now() / 1000) + (hours * 60 * 60),
        data: user,
    }, secret);
    return `Bearer ${token}`;
}

function verifyJWTToken(req, res, next) {
    var token = getToken(req);
    if (token) {
        JWT.verify(token, secret, function(err, decoded) {
            if (err) {
                return res.json({
                    success: false,
                    message: 'Failed to authenticate token.'
                });
            } else {
                req.decoded = decoded;
                next();
            }
        });
    } else {
        return res.status(403).send({
            success: false,
            message: 'No token provided.'
        });
    }
};



function getToken(req) {
    if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
        return req.headers.authorization.split(' ')[1];
    } else if (req.query && req.query.token) {
        return req.query.token;
    }
    return null;
}